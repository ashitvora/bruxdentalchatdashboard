<?php

use App\Organization;
use Faker\Generator as Faker;

$factory->define(Organization::class, function (Faker $faker) {
	return [
		'name' => $faker->company . "-" . $faker->city . "({$faker->state})",
	];
});
