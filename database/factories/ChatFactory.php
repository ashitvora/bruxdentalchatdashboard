<?php

use App\Chat;
use Faker\Generator as Faker;

$factory->define(Chat::class, function (Faker $faker) {
	return [
		'patient' => $faker->name,
		'dob' => $faker->date($format = 'Y-m-d', $max = 'now'),
		'insurer' => $faker->company,
		'email' => $faker->safeEmail,
		'phone' => $faker->phoneNumber,
		'appointment_reason' => $faker->sentence,
		'past_medical_history' => implode(" ", $faker->sentences(3)),
		'transcript' => implode("", $faker->paragraphs(10)),
		'status' => array_random([Chat::SCHEDULED, Chat::NOT_SCHEDULED, Chat::FOLLOW_UP_REQUIRED], 1)[0],
	];
});
