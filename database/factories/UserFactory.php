<?php

use App\User;
use Faker\Generator as Faker;

$factory->define(User::class, function (Faker $faker) {
	return [
		'name' => $faker->name,
		'email' => $faker->unique()->safeEmail,
		'password' => 'P@$$w0rd', // secret
		'type' => array_random([User::AGENT_ROLE, User::CUSTOMER_ROLE]),
		'remember_token' => str_random(10),
	];
});
