<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {
	/**
	 * Seed the application's database.
	 *
	 * @return void
	 */
	public function run() {
		// Create Users
		$users = factory(\App\User::class, 10)->create();
		$organizations = factory(\App\Organization::class, 10)->create();

		$users->each(function ($user) {
			$user->organizations()->sync(array_random(\App\Organization::all()->pluck('id')->toArray(), 3));
		});

		for ($i = 0; $i < 100; $i++) {
			$chats = factory(\App\Chat::class)->create([
				'user_id' => $users->random(1)->pluck('id')->first(), // array_random($users->pluck('id')->toArray(), 1)[0],
				'organization_id' => $organizations->random(1)->pluck('id')->first(), //array_random($organizations->pluck('id')->toArray(), 1)[0],
			]);
		}

	}
}
