<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChatsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('chats', function (Blueprint $table) {
			$table->increments('id');
			$table->string('patient');
			$table->date('dob')->nullable();
			$table->string('insurer')->nullable();
			$table->string('email')->nullable();
			$table->string('phone')->nullable();
			$table->string('appointment_reason', 1024)->nullable();
			$table->text('past_medical_history')->nullable();
			$table->text('transcript');
			$table->text('status');
			$table->integer('organization_id')->unsigned();
			$table->integer('user_id')->unsigned();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('chats');
	}
}
