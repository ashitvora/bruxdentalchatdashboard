<?php

namespace App\Http\ViewComposers;

use App\Organization;
use Illuminate\View\View;

class SidebarComposer {

	/**
	 * Bind data to the view.
	 *
	 * @param  View  $view
	 * @return void
	 */
	public function compose(View $view) {
		if (auth()->user()->isAdmin or auth()->user()->isAgent) {
			$orgs = Organization::all();
		} else {
			$orgs = auth()->user()->organizations;
		}

		$view->with('orgs', $orgs);
	}
}