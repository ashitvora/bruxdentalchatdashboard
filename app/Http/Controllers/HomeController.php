<?php

namespace App\Http\Controllers;

use App\Http\Requests\SearchStatsRequest;
use App\Organization;

class HomeController extends Controller {

	public function index(SearchStatsRequest $request) {
		// Find all the organizations user has access to.
		if (auth()->user()->isAdmin or auth()->user()->isAgent) {
			$orgs = Organization::with(['chats' => function ($q) use ($request) {
				if ($request->has('from')) {
					$q->where("created_at", '>=', $request->get("from"))->where("created_at", '<=', $request->get("to"));
				}
			}])->get();
		} else {
			$orgs = auth()->user()->organizations()->with(['chats' => function ($q) use ($request) {
				if ($request->has('to')) {
					$q->where("created_at", '>=', $request->get("from"))->where("created_at", '<=', $request->get("to"));
				}
			}])->get();
		}

		return view('home', compact('orgs'));
	}
}
