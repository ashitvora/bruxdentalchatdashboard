<?php

namespace App\Http\Controllers;

use App\Chat;
use App\Http\Requests\Chat\CreateChat;
use App\Organization;
use Illuminate\Http\Request;

class ChatController extends Controller {

	public function all() {
		// Get chat of all the organizations that user has access to
	}

	public function index(Organization $organization) {
		$chats = $organization->chats()->latest()->get();

		return view("chats.index", compact("chats", "organization"));
	}

	public function create(Organization $organization) {
		return view("chats.create", ["chat" => new Chat, "organization" => $organization]);
	}

	public function store(CreateChat $request, Organization $organization) {
		$chat = new Chat($request->all());
		$chat->organization_id = $organization->id;

		auth()->user()->chats()->save($chat);

		return redirect()->route("organizations.chats.index", $organization->id);
	}

	public function edit(Request $request, Chat $chat, Organization $organization) {
		return view("chats.edit", compact('chat', 'organization'));
	}

	public function update(Request $request, Chat $chat, Organization $organization) {
		$chat->update($request->all());

		return redirect()->route("organizations.chats.index", $organization->id);
	}

	public function destroy(Chat $chat, Organization $organization) {
		$chat->delete();

		return redirect()->route("organizations.chats.index", $organization->id);
	}
}
