<?php

namespace App\Http\Controllers;

use App\Organization;
use App\User;
use Illuminate\Http\Request;

class OrganizationsController extends Controller {

	public function index() {
		$orgs = Organization::all();

		return view("organizations.index", compact("orgs"));
	}

	public function create() {
		return view("organizations.create", ["organization" => new Organization]);
	}

	public function store(Request $request) {
		$org = new Organization($request->all());

		$org->save();

		return redirect()->route("organizations.index");
	}

	public function edit(Request $request, Organization $organization) {
		return view("organizations.edit", compact('organization'));
	}

	public function update(Request $request, Organization $organization) {
		$organization->update($request->all());

		return redirect()->route("organizations.index");
	}

	public function destroy(Organization $organization) {
		$organization->delete();

		return redirect()->route("organizations.index");
	}

	public function assignUsers(Request $request, Organization $organization) {
		$users = User::whereType(User::CUSTOMER_ROLE)->get();
		$user_orgs = $organization->users->pluck('id')->toArray();

		return view("organizations.assign_users", compact("users", "organization", "user_orgs"));
	}

	public function updateUsers(Request $request, Organization $organization) {
		$organization->users()->sync($request->get('users', []));

		return redirect()->route("organizations.index");
	}
}
