<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\CreateUser;
use App\Http\Requests\User\DeleteUser;
use App\Http\Requests\User\UpdateUser;
use App\Organization;
use App\User;
use Illuminate\Http\Request;

class UsersController extends Controller {
	public function index() {
		$users = User::all();

		return view("users.index", compact("users"));
	}

	public function create() {
		return view("users.create", ["user" => new User]);
	}

	public function store(CreateUser $request) {
		$org = new User($request->all());

		$org->save();

		return redirect()->route("users.index");
	}

	public function edit(Request $request, User $user) {
		return view("users.edit", compact('user'));
	}

	public function update(UpdateUser $request, User $user) {
		$user->update($request->all());

		return redirect()->route("users.index");
	}

	public function destroy(DeleteUser $request, User $user) {
		$user->delete();

		return redirect()->route("users.index");
	}

	public function assignOrganizations(Request $request, User $user) {
		$orgs = Organization::all();
		$user_orgs = $user->organizations->pluck('id')->toArray();

		return view("users.assign_organizations", compact("orgs", "user", "user_orgs"));
	}

	public function updateOrganizations(Request $request, User $user) {
		$user->organizations()->sync($request->get('organizations', []));

		return redirect()->route("users.index");
	}
}
