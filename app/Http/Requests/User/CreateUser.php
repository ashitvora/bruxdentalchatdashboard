<?php

namespace App\Http\Requests\User;

use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreateUser extends FormRequest {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return auth()->user()->isAdmin;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return [
			"name" => ["required", "min:3", "max:255"],
			"email" => ["required", "email", "unique:users", "min:3", "max:255"],
			"type" => ['required', Rule::in([User::ADMIN_ROLE, User::AGENT_ROLE, User::CUSTOMER_ROLE])],
			"password" => ["required", "min:8", "max:20"],
			"password_again" => ["same:password"],
		];
	}
}
