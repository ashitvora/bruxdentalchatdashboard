<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider {
	public function boot() {
		View::composer(
			'layouts.sidebar', 'App\Http\ViewComposers\SidebarComposer'
		);

	}

	public function register() {
		//
	}
}