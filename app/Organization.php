<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organization extends Model {
	protected $fillable = [
		'name',
	];

	public function users() {
		return $this->belongsToMany(User::class);
	}

	public function chats() {
		return $this->hasMany(Chat::class);
	}
}
