<?php

namespace App;

use Hash;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable {

	const ADMIN_ROLE = "ADMIN";
	const AGENT_ROLE = "AGENT";
	const CUSTOMER_ROLE = "CUSTOMER";

	use Notifiable;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name', 'email', 'password', 'type',
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password', 'remember_token',
	];

	public function getIsAdminAttribute($value) {
		return $this->attributes['type'] === self::ADMIN_ROLE;
	}

	public function getIsAgentAttribute($value) {
		return $this->attributes['type'] === self::AGENT_ROLE;
	}

	public function setPasswordAttribute($value) {
		$this->attributes['password'] = Hash::make($value);
	}

	public function chats() {
		return $this->hasMany(Chat::class);
	}

	public function organizations() {
		return $this->belongsToMany(Organization::class);
	}

}
