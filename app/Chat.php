<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Chat extends Model {

	const FOLLOW_UP_REQUIRED = "FOLLOW UP REQUIRED";
	const SCHEDULED = "SCHEDULED";
	const NOT_SCHEDULED = "NOT SCHEDULED";

	protected $fillable = [
		'organization_id',
		'patient',
		'dob',
		'insurer',
		'email',
		'phone',
		'appointment_reason',
		'past_medical_history',
		'transcript',
		'status',
	];

	public function setDobAttribute($value) {
		$this->attributes['dob'] = Carbon::parse($value);
	}

	public function organization() {
		return $this->belongsTo(Organization::class);
	}

	public function agent() {
		return $this->hasOne(User::class, "user_id");
	}

}
