<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class AddAdminUser extends Command {
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'make:admin';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Create an Admin User for the app';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle() {
		$email = "a.k.vora@gmail.com";
		$password = str_random(20);

		$user = new \App\User([
			"name" => "Admin",
			"email" => $email,
			"type" => \App\User::ADMIN_ROLE,
			"password" => bcrypt($password),
		]);

		$user->save();

		$this->info("User create. Email: {$email} & Password: {$password}");
	}
}
