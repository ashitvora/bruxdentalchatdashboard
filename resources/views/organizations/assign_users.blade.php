@extends('layouts.app')

@section("page_title", "Grant Customers Access to {$organization->name}")

@section('breadcrumbs')
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">Home</a></li>
    <li class="breadcrumb-item"><a href="{{ route("organizations.index") }}">Organizations</a></li>
    <li class="breadcrumb-item active">Grant Customers Access to {{ $organization->name }}</li>
</ol>
@stop

@section("content")
<div class="card">
    <div class="card-body">
        {{ Form::model($organization, ['route' => ['organizations.assign-users', $organization]], ["class" => "form"]) }}

            @foreach($users as $user)
            <div class="form-group" style="margin-bottom: 0;">
                <label class="checkbox">
                    {{ Form::checkbox('users[]', $user->id, in_array($user->id, $user_orgs), ['class' => 'checkbox']) }}
                    {{ $user->name }}
                </label>
            </div>
            @endforeach

            <button class="btn btn-primary">Save changes</button>
            <a href="{{ route("organizations.index") }}" class="btn btn-link">Cancel</a>

        {{ Form::close() }}
    </div>
</div>
@stop