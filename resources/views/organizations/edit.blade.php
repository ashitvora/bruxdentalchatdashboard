@extends('layouts.app')


@section("page_title", "Edit Organizations - {$organization->name}")

@section('breadcrumbs')
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">Home</a></li>
    <li class="breadcrumb-item"><a href="{{ route("organizations.index") }}">Organizations</a></li>
    <li class="breadcrumb-item active">{{ $organization->name }}</li>
</ol>
@stop

@section("content")
<div class="card">
    <div class="card-body">
        {{ Form::model($organization, ['route' => ['organizations.update', $organization->id]], ["class" => "form"]) }}

            {{ Form::hidden("_method", "PUT") }}

            <div class="form-group">
                {{ Form::label('name', 'Name of the organization') }}
                {{ Form::text('name', null, ['class' => 'form-control']) }}
            </div>

            <button class="btn btn-primary">Save Changes</button>
            <a href="{{ route("organizations.index") }}" class="btn btn-link">Cancel</a>

        {{ Form::close() }}
    </div>
</div>
@stop