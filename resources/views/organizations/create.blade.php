@extends('layouts.app')

@section("page_title", "Add New Organizations")

@section('breadcrumbs')
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">Home</a></li>
    <li class="breadcrumb-item"><a href="{{ route("organizations.index") }}">Organizations</a></li>
    <li class="breadcrumb-item active">New Organization</li>
</ol>
@stop

@section("content")
<div class="card">
    <div class="card-body">
        {{ Form::model($organization, ['route' => ['organizations.store']], ["class" => "form"]) }}

            <div class="form-group">
                {{ Form::label('name', 'Name') }}
                {{ Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'LuckySmile, San Diego CA']) }}
            </div>

            <button class="btn btn-primary">Add this organization</button>
            <a href="{{ route("organizations.index") }}" class="btn btn-link">Cancel</a>

        {{ Form::close() }}
    </div>
</div>

@stop