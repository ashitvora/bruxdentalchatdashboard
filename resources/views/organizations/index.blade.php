@extends('layouts.app')

@section("page_title", "Organizations")

@section('breadcrumbs')
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">Home</a></li>
    <li class="breadcrumb-item active">Organizations</li>
</ol>
@stop

@section("content")

    @if(!is_null($orgs) and $orgs->count() > 0)

    <div class="card">

        <div class="card-title">
            <div class="row">
                <div class="col-md-4">
                    <form action="{{ route("organizations.index") }}">
                        <div class="form-group">
                            <input type="text" class="form-control" name="q" placeholder="Search organization..." value="{{ Request::get("q", "") }}">
                        </div>
                    </form>
                </div>
                <div class="col-md-8 text-right">
                    <a href="{{ route('organizations.create') }}" class="btn btn-link">Add new organization</a>
                </div>
            </div>
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th></th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($orgs as $org)
                        <tr>
                            <td>{{ $org->name }}</td>
                            <td>
                                <ul class="list-inline list-unstyled">
                                    <li class="list-inline-item">
                                        <a href="{{ route("organizations.edit", $org->id) }}">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a href="{{ route("organizations.assign-users", $org->id) }}">
                                            <i class="fa fa-user-md"></i>
                                        </a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a href="{{ route("organizations.chats.index", $org->id) }}">
                                            <i class="fa fa-comments"></i>
                                        </a>
                                    </li>
                                    <li class="list-inline-item">
                                        {{ Form::model($org, ['route' => ['organizations.destroy', $org->id], "onClick" => "javascript:return window.confirm('Are you sure?')"]) }}
                                            {{ Form::hidden("_method", "DELETE") }}
                                            <button class="btn btn-link">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        {{ Form::close() }}
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>


    @else

        <div class="text-center">
            <p class="lead">There are no organizations</p>
            <a href="{{ route('organizations.create') }}" class="btn btn-primary">Create first organization</a>
        </div>

    @endif

@stop