@extends('layouts.app')

@section("page_title", "Chat for {$organization->name}")

@section('breadcrumbs')
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">Home</a></li>
    <li class="breadcrumb-item"><a href="{{ route('organizations.index') }}">Organizations</a></li>
    <li class="breadcrumb-item active">Chats for {{ $organization->name }}</li>
</ol>
@stop

@section("content")

    @if(!is_null($chats) and $chats->count() > 0)
        <div class="card-title">
            <div class="row">
                <div class="col-md-4">
                    <form action="{{ route("organizations.chats.index", $organization->id) }}">
                        <div class="form-group">
                            <input type="text" class="form-control" name="q" placeholder="Search chat..." value="{{ Request::get("q", "") }}">
                        </div>
                    </form>
                </div>

                <div class="col-md-8 text-right">
                    <a href="{{ route('organizations.chats.create', $organization->id) }}" class="btn btn-link">Add new chat</a>
                </div>
            </div>
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Patient</th>
                            <th>Email</th>
                            <th>Phone #</th>
                            <th>DoB</th>
                            <th>Insurer</th>
                            <th>Appointment Reason</th>
                            <th></th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($chats as $chat)
                        <tr>
                            <td><span class="badge badge-{{ str_slug($chat->status) }}">{{ $chat->status }}</span></td>
                            <td>{{ $chat->patient }}</td>
                            <td>{{ $chat->email }}</td>
                            <td>{{ $chat->phone }}</td>
                            <td>{{ $chat->dob }}</td>
                            <td>{{ $chat->insurer }}</td>
                            <td>{{ $chat->appointment_reason }}</td>
                            <td>
                                <ul class="list-inline list-unstyled">
                                    <li class="list-inline-item">
                                        <a href="#" data-chat-trigger="chat-{{ $chat->id }}">
                                            <i class="fa fa-ellipsis-h"></i>
                                        </a>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr class="hidden" data-chat-container="chat-{{ $chat->id }}">
                            <td colspan="8" class="text-left">
                                <pre style="height: 500px; white-space: pre-wrap; word-wrap: break-word; font-family: monospace !important; font-size: 1.3rem;">{{ $chat->transcript }}</pre>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>


    @else

        <div class="text-center">
            <p>There are no chats for <strong>{{ $organization->name }}</strong></p>
            <a href="{{ route('organizations.chats.create', $organization->id) }}" class="btn btn-primary">Create first chat</a>
        </div>

    @endif
@stop