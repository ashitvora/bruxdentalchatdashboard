@extends('layouts.app')


@section("page_title", "New Chat for {$organization->name}")

@section('breadcrumbs')
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">Home</a></li>
    <li class="breadcrumb-item"><a href="{{ route("organizations.chats.index", $organization->name) }}">Chats for {{ $organization->name }}</a></li>
    <li class="breadcrumb-item active">New Chat</li>
</ol>
@stop

@section("content")
<div class="card">
    <div class="card-body">
        {{ Form::model($chat, ['route' => ['organizations.chats.store', $organization->id]], ["class" => "form"]) }}

            <div class="row">
                <div class="col-4">
                    <div class="form-group {{ $errors->has('patient') ? 'has-error' : '' }}">
                        {{ Form::label('patient', 'Patient') }}
                        {{ Form::text('patient', null, ['class' => 'form-control', 'placeholder' => 'Jack Appleseed', 'autofocus' => true]) }}

                        @foreach ($errors->get('patient', []) as $error)
                            <p class="text-danger">{{ $error }}</p>
                        @endforeach
                    </div>
                </div>

                <div class="col-4">
                    <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                        {{ Form::label('email', 'Email address') }}
                        {{ Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'mike@gmail.com']) }}

                        @foreach ($errors->get('email', []) as $error)
                            <p class="text-danger">{{ $error }}</p>
                        @endforeach
                    </div>
                </div>

                <div class="col-2">
                    <div class="form-group {{ $errors->has('phone') ? 'has-error' : '' }}">
                        {{ Form::label('phone', 'Phone #') }}
                        {{ Form::text('phone', null, ['class' => 'form-control', 'placeholder' => '8585 123 456']) }}

                        @foreach ($errors->get('phone', []) as $error)
                            <p class="text-danger">{{ $error }}</p>
                        @endforeach
                    </div>
                </div>

                <div class="col-2">
                    <div class="form-group {{ $errors->has('dob') ? 'has-error' : '' }}">
                        {{ Form::label('dob', 'Date of Birth') }}
                        {{ Form::text('dob', null, ['class' => 'form-control', 'placeholder' => '10/17/1980', "data-provide" => "datepicker", "data-date-format" => "mm/dd/yyyy"]) }}

                        @foreach ($errors->get('dob', []) as $error)
                            <p class="text-danger">{{ $error }}</p>
                        @endforeach
                    </div>
                </div>

            </div>


            <div class="row">
                <div class="col-6">
                    <div class="form-group {{ $errors->has('appointment_reason') ? 'has-error' : '' }}">
                        {{ Form::label('appointment_reason', 'Reason for Appointment') }}
                        {{ Form::text('appointment_reason', null, ['class' => 'form-control', 'placeholder' => '']) }}

                        @foreach ($errors->get('appointment_reason', []) as $error)
                            <p class="text-danger">{{ $error }}</p>
                        @endforeach
                    </div>
                </div>


                <div class="col-3">
                    <div class="form-group {{ $errors->has('insurer') ? 'has-error' : '' }}">
                        {{ Form::label('insurer', 'Insurer') }}
                        {{ Form::text('insurer', null, ['class' => 'form-control', 'placeholder' => 'Metlife']) }}

                        @foreach ($errors->get('insurer', []) as $error)
                            <p class="text-danger">{{ $error }}</p>
                        @endforeach
                    </div>
                </div>


                <div class="col-3">
                    <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                        {{ Form::label('status', 'Status') }}
                        {{ Form::select('status', [\App\Chat::SCHEDULED => "Appointment Scheduled", \App\Chat::NOT_SCHEDULED => "Appointment NOT Scheduled", \App\Chat::FOLLOW_UP_REQUIRED => "Follow-up Required"], null, ['class' => 'form-control', 'placeholder' => 'Status']) }}

                        @foreach ($errors->get('status', []) as $error)
                            <p class="text-danger">{{ $error }}</p>
                        @endforeach
                    </div>
                </div>
            </div>


            <div class="form-group {{ $errors->has('past_medical_history') ? 'has-error' : '' }}">
                {{ Form::label('past_medical_history', 'Past Medical History') }}
                {{ Form::textarea('past_medical_history', null, ['class' => 'form-control', 'placeholder' => '', "rows" => 3]) }}

                @foreach ($errors->get('past_medical_history', []) as $error)
                    <p class="text-danger">{{ $error }}</p>
                @endforeach
            </div>

            <div class="form-group {{ $errors->has('transcript') ? 'has-error' : '' }}">
                {{ Form::label('transcript', 'Chat Transcript') }}
                {{ Form::textarea('transcript', null, ['class' => 'form-control', 'placeholder' => '', "rows" => 15, "style" => "height: auto;"]) }}

                @foreach ($errors->get('transcript', []) as $error)
                    <p class="text-danger">{{ $error }}</p>
                @endforeach
            </div>



            <button class="btn btn-primary">Save</button>
            <a href="{{ route("organizations.chats.index", $organization->id) }}" class="btn btn-link">Cancel</a>

        {{ Form::close() }}
    </div>
</div>
@stop