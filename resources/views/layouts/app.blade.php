<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield("page_title") - {{ config('app.name', 'Brux Dental Chat') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">

    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet">
    <link href="{{ asset('css/lib/bootstrap/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/lib/datepicker/bootstrap-datepicker3.min.css') }}" rel="stylesheet">
    {{-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> --}}
    <link href="{{ asset('css/helper.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

    <style type="text/css">
        * { font-family: 'Open Sans', sans-serif !important;  }
        .fa{
            font-family: 'fontawesome' !important;
        }

        [class^="ti-"], [class*=" ti-"]{
            font-family: 'themify' !important;
        }

        ::placeholder { color: #ccc !important; }
        .badge{
            color: white !important;
        }

        .badge-admin, .badge-not-scheduled{
            background-color: #ef5350;
        }

        .badge-agent, .badge-scheduled{
            background-color: #26dad2;
        }

        .badge-customer, .badge-follow-up-required{
            background-color: #5c4ac7;
        }

        .ellipsis{
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
        }
    </style>
</head>
<body>
    <!-- Main wrapper  -->
    <div id="main-wrapper">
        <!-- header header  -->
        <div class="header">
            @include("layouts.navbar")
        </div>
        <!-- End header header -->
        <!-- Left Sidebar  -->
        <div class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                @include("layouts.sidebar")
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </div>
        <!-- End Left Sidebar  -->
        <!-- Page wrapper  -->
        <div class="page-wrapper">
            <!-- Bread crumb -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-primary">@yield("page_title")</h3> </div>
                <div class="col-md-7 align-self-center">
                    @yield('breadcrumbs')
                </div>
            </div>
            <!-- End Bread crumb -->
            <!-- Container fluid  -->
            <div class="container-fluid">
                @yield('content')
            </div>
            <!-- End Container fluid  -->
            <!-- footer -->
            <footer class="footer"> © {{ date('Y') }} All rights reserved. {{ config('app.name') }}</footer>
            <!-- End footer -->
        </div>
        <!-- End Page wrapper  -->
    </div>
    <!-- End Wrapper -->


    <script src="{{ asset('js/lib/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{ asset('js/lib/bootstrap/js/popper.min.js') }}"></script>
    <script src="{{ asset('js/lib/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/lib/datepicker/bootstrap-datepicker.min.js') }}"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{ asset('js/jquery.slimscroll.js') }}"></script>
    <!--Menu sidebar -->
    <script src="{{ asset('js/sidebarmenu.js') }}"></script>

    <script src="{{ asset('js/lib/chart-js/Chart.bundle.js') }}"></script>

    <script type="text/javascript">
        $('[data-toggle="popover"]').popover({});

        $('[data-chat-trigger]').on('click', function(e){
            e.preventDefault();

            var id = $(this).data('chat-trigger');
            var currentVisibleChatContainer = $('[data-chat-container="'+ id +'"]:visible').data("chat-container");

            $('[data-chat-container]').addClass('hidden');

            if(id != currentVisibleChatContainer){
                $('[data-chat-container="'+ id +'"]').removeClass('hidden');
            }

        });
    </script>

    @yield("scripts")
</body>
</html>
