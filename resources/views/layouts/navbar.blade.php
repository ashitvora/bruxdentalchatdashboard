<nav class="navbar top-navbar navbar-expand-md navbar-light">
    <!-- Logo -->
    <div class="navbar-header">
        <a class="navbar-brand" href="/">
            <!-- Logo icon -->
           <b><img src="/images/logo.png" alt="homepage" class="dark-logo" /></b>
           <!--End Logo icon -->
           <!-- Logo text -->
           <span><img src="/images/logo-text.png" alt="homepage" class="dark-logo" /></span>
        </a>
    </div>
    <!-- End Logo -->
    <div class="navbar-collapse">
        <ul class="navbar-nav mr-auto mt-md-0">
        </ul>


        <!-- User profile and search -->
        <ul class="navbar-nav my-lg-0">
            <!-- Search -->
            <li class="nav-item hidden-sm-down search-box">
                <a class="nav-link hidden-sm-down text-muted  " href="javascript:void(0)"><i class="ti-search"></i></a>
                <form class="app-search">
                    <input type="text" class="form-control" placeholder="Search here">
                    <a class="srh-btn"><i class="ti-close"></i></a>
                </form>
            </li>


            <!-- Profile -->
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle text-muted  " href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img src="/images/users/5.jpg" alt="user" class="profile-pic" />
                </a>
                <div class="dropdown-menu dropdown-menu-right animated zoomIn">
                    <ul class="dropdown-user">
                        <li><a href="#"><i class="ti-user"></i> Profile</a></li>
                        <li><a href="#"><i class="ti-wallet"></i> Balance</a></li>
                        <li><a href="#"><i class="ti-email"></i> Inbox</a></li>
                        <li><a href="#"><i class="ti-settings"></i> Setting</a></li>
                        <li><a href="#"><i class="fa fa-power-off"></i> Logout</a></li>
                    </ul>
                </div>
            </li>
        </ul>
    </div>
</nav>