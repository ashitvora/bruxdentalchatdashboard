<nav class="sidebar-nav">
    <ul id="sidebarnav">
        <li>
            <a href="/" aria-expanded="false">
                <i class="fa fa-tachometer"></i>
                <span class="hide-menu">Dashboard</span>
            </a>
        </li>

        <li>
            <a href="{{ route('users.index') }}" aria-expanded="false">
                <i class="fa fa-user-md"></i>
                <span class="hide-menu">Users</span>
            </a>
        </li>


        <li>
            <a href="{{ route('organizations.index') }}" aria-expanded="false">
                <i class="fa fa-hospital-o"></i>
                <span class="hide-menu">Organizations</span>
            </a>
        </li>

        <li class="nav-devider"></li>
        <li class="nav-label">Organizations</li>

        @foreach($orgs as $org)
        <li>
            <a class="has-arrow ellipsis" href="{{ route('organizations.chats.index', $org->id) }}">{{ $org->name }}</a>
        </li>
        @endforeach
    </ul>
</nav>