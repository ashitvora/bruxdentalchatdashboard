@extends('layouts.app')

@section('content')

<div>
    <form class="form">
        <div class="row">
            <div class="col-4">
                <div class="form-group {{ $errors->has('from') ? 'has-error' : '' }}">
                    {{ Form::label('from', 'From') }}
                    {{ Form::text('from', null, ['class' => 'form-control', "data-provide" => "datepicker", "data-date-format" => "mm/dd/yyyy", 'autofocus' => true]) }}

                    @foreach ($errors->get('from', []) as $error)
                        <p class="text-danger">{{ $error }}</p>
                    @endforeach
                </div>
            </div>
            <div class="col-4">
                <div class="form-group {{ $errors->has('to') ? 'has-error' : '' }}">
                    {{ Form::label('to', 'To') }}
                    {{ Form::text('to', null, ['class' => 'form-control', "data-provide" => "datepicker", "data-date-format" => "mm/dd/yyyy"]) }}

                    @foreach ($errors->get('to', []) as $error)
                        <p class="text-danger">{{ $error }}</p>
                    @endforeach
                </div>
            </div>
            <div class="col-4">
                <button class="btn btn-primary" style="margin-top: 35px;">Search</button>
            </div>
        </div>
    </form>
</div>



<div class="row">
@foreach($orgs as $org)
    <div class="col-sm-6 col-md-4">
        <div class="card">
            <div class="card-title">
                <h4 class="text-center">{{ $org->name }} - <strong>{{ $org->chats->count() }}</strong></h4>
            </div>
            <div class="card-body">
            @if($org->chats->count())
                <canvas id="pie-chart-{{ $org->id }}"></canvas>
            @else
                <div class="text-info text-center mt-5">There are no chats for this Dental Office yet.</div>
            @endif
            </div>
        </div>
    </div>

    {{-- <div class="col-md-3">
        <div class="card bg-primary p-20">
            <div class="media widget-ten">
                <div class="media-left meida media-middle">
                    <span><i class="fa fa-users fa-3x"></i></span>
                </div>
                <div class="media-body media-text-right">
                    <h2 class="color-white">{{ $org->chats->count() }}</h2>
                    <p class="m-b-0">Visitors</p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card bg-pink p-20">
            <div class="media widget-ten">
                <div class="media-left meida media-middle">
                    <span><i class="fa fa-user-plus fa-3x"></i></span>
                </div>
                <div class="media-body media-text-right">
                    <h2 class="color-white">{{ $org->chats->where('type', 'SCHEDULED')->count() }}</h2>
                    <p class="m-b-0">Appointments Scheduled</p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card bg-success p-20">
            <div class="media widget-ten">
                <div class="media-left meida media-middle">
                    <span><i class="fa fa-line-chart fa-3x"></i></span>
                </div>
                <div class="media-body media-text-right">
                    <h2 class="color-white">{{ $org->chats->count() ? $org->chats->where('type', 'SCHEDULED')->count() / $org->chats->count() * 100 : 0 }}%</h2>
                    <p class="m-b-0">Conversion Rate</p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card bg-danger p-20">
            <div class="media widget-ten">
                <div class="media-left meida media-middle">
                    <span><i class="fa fa-money fa-3x"></i></span>
                </div>
                <div class="media-body media-text-right">
                    <h2 class="color-white">${{ $org->chats->where('type', 'SCHEDULED')->count() * 1000 }}</h2>
                    <p class="m-b-0">Life Time Value</p>
                </div>
            </div>
        </div>
    </div> --}}
<hr>
@endforeach
</div>

@stop



@section("scripts")
<script type="text/javascript">
@foreach($orgs as $org)
@if($org->chats->count())
var ctx = document.getElementById("pie-chart-{{ $org->id }}");
ctx.height = 300;
var myChart = new Chart( ctx, {
    type: 'pie',
    data: {
        datasets: [ {
            data: [
                {{ $org->chats->where('status', 'SCHEDULED')->count() }},
                {{ $org->chats->where('status', 'NOT_SCHEDULED')->count() }},
                {{ $org->chats->where('status', 'FOLLOW_UP')->count() }}
            ],
            backgroundColor: [
                "rgba({{ abs(($org->id + strlen($org->name)) % 255) }}, {{ abs(($org->id - strlen($org->name)) % 255) }}, {{ abs(($org->id * strlen($org->name)) % 255) }}, 0.9)",
                "rgba({{ abs(($org->id + strlen($org->name)) % 255) }}, {{ abs(($org->id - strlen($org->name)) % 255) }}, {{ abs(($org->id * strlen($org->name)) % 255) }}, 0.6)",
                "rgba({{ abs(($org->id + strlen($org->name)) % 255) }}, {{ abs(($org->id - strlen($org->name)) % 255) }}, {{ abs(($org->id * strlen($org->name)) % 255) }}, 0.3)",
            ],
            hoverBackgroundColor: [
                "rgba({{ abs(($org->id + strlen($org->name)) % 255) }}, {{ abs(($org->id - strlen($org->name)) % 255) }}, {{ abs(($org->id * strlen($org->name)) % 255) }}, 0.9)",
                "rgba({{ abs(($org->id + strlen($org->name)) % 255) }}, {{ abs(($org->id - strlen($org->name)) % 255) }}, {{ abs(($org->id * strlen($org->name)) % 255) }}, 0.6)",
                "rgba({{ abs(($org->id + strlen($org->name)) % 255) }}, {{ abs(($org->id - strlen($org->name)) % 255) }}, {{ abs(($org->id * strlen($org->name)) % 255) }}, 0.3)",
            ]

        } ],
        labels: [
            "Scheduled",
            "Not Scheduled",
            "Follow-up Required"
        ]
},
    options: {
        responsive: true
    }
} );
@endif
@endforeach
</script>
@stop