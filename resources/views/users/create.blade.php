@extends('layouts.app')

@section("page_title", "Add New User")

@section('breadcrumbs')
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">Home</a></li>
    <li class="breadcrumb-item"><a href="{{ route("users.index") }}">Users</a></li>
    <li class="breadcrumb-item active">Add New User</li>
</ol>
@stop


@section("content")
<div class="card">
    <div class="card-body">
        {{ Form::model($user, ['route' => ['users.store']], ["class" => "form"]) }}


            <div class="row">
                <div class="col-4">
                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                        {{ Form::label('name', 'Name of the user') }}
                        {{ Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Jack Appleseed', 'autofocus' => true]) }}

                        @foreach ($errors->get('name', []) as $error)
                            <p class="text-danger">{{ $error }}</p>
                        @endforeach
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                        {{ Form::label('email', 'Email address') }}
                        {{ Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'person_name@company.com']) }}

                        @foreach ($errors->get('email', []) as $error)
                            <p class="text-danger">{{ $error }}</p>
                        @endforeach
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group {{ $errors->has('type') ? 'has-error' : '' }}">
                        {{ Form::label('type', 'User Role') }}
                        {{ Form::select('type', [\App\User::CUSTOMER_ROLE => "Customer", \App\User::AGENT_ROLE => "Support Agent", \App\User::ADMIN_ROLE => "Admin"], null, ['class' => 'form-control', 'placeholder' => 'Select User Role']) }}

                        @foreach ($errors->get('type', []) as $error)
                            <p class="text-danger">{{ $error }}</p>
                        @endforeach
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-4">
                    <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                        {{ Form::label('password', 'Password') }}
                        {{ Form::text('password', null, ['class' => 'form-control', 'autocomplete' => "off", 'placeholder' => 'Easy to remember; hard to guess']) }}

                        @foreach ($errors->get('password', []) as $error)
                            <p class="text-danger">{{ $error }}</p>
                        @endforeach
                    </div>

                </div>
                <div class="col-4">
                    <div class="form-group {{ $errors->has('password_again') ? 'has-error' : '' }}">
                        {{ Form::label('password_again', 'Password again') }}
                        {{ Form::password('password_again', ['class' => 'form-control', 'autocomplete' => "off", 'placeholder' => 'Should be same as above']) }}

                        @foreach ($errors->get('password_again', []) as $error)
                            <p class="text-danger">{{ $error }}</p>
                        @endforeach
                    </div>
                </div>
            </div>

            <button class="btn btn-primary">Add this user</button>
            <a href="{{ route("users.index") }}" class="btn btn-link">Cancel</a>

        {{ Form::close() }}
    </div>
</div>
@stop