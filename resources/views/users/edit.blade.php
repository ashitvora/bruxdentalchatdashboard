@extends('layouts.app')

@section("content")
<div class="container">
{{ Form::model($user, ['route' => ['users.update', $user->id]], ["class" => "form"]) }}

    {{ Form::hidden("_method", "PUT") }}

    <div class="row">
        <div class="col-4">
            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                {{ Form::label('name', 'Name of the user') }}
                {{ Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Jack Appleseed', 'autofocus' => true]) }}

                @foreach ($errors->get('name', []) as $error)
                    <p class="text-danger">{{ $error }}</p>
                @endforeach
            </div>
        </div>
        <div class="col-4">
            <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                {{ Form::label('email', 'Email address') }}
                {{ Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'person_name@company.com']) }}

                @foreach ($errors->get('email', []) as $error)
                    <p class="text-danger">{{ $error }}</p>
                @endforeach
            </div>
        </div>
        <div class="col-4">
            <div class="form-group {{ $errors->has('type') ? 'has-error' : '' }}">
                {{ Form::label('type', 'User Role') }}
                {{ Form::select('type', [\App\User::CUSTOMER_ROLE => "Customer", \App\User::AGENT_ROLE => "Support Agent", \App\User::ADMIN_ROLE => "Admin"], null, ['class' => 'form-control', 'placeholder' => 'Select User Role']) }}

                @foreach ($errors->get('type', []) as $error)
                    <p class="text-danger">{{ $error }}</p>
                @endforeach
            </div>
        </div>
    </div>

    <button class="btn btn-primary">Save changes</button>
    <a href="{{ route("users.index") }}" class="btn btn-link">Cancel</a>

{{ Form::close() }}
</div>
@stop