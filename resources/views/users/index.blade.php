@extends('layouts.app')


@section("page_title", "Users")

@section('breadcrumbs')
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">Home</a></li>
    <li class="breadcrumb-item active">Users</li>
</ol>
@stop

@section("content")
    @if(!is_null($users) and $users->count() > 0)

        <div class="card">
            <div class="card-title">
                <div class="row">
                    <div class="col-md-4">
                        <form action="{{ route("users.index") }}">
                            <div class="form-group">
                                <input type="text" class="form-control" name="q" placeholder="Search user..." value="{{ Request::get("q", "") }}">
                            </div>
                        </form>
                    </div>
                    <div class="col-md-8 text-right">
                        <a href="{{ route('users.create') }}" class="btn btn-link">Add new user</a>
                    </div>
                </div>
            </div>

            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Name</th>
                                <th>Email</th>
                                <th></th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach($users as $user)
                            <tr>
                                <td>
                                    <span class="badge badge-{{ strtolower($user->type) }}">{{ $user->type }}</span>
                                </td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>
                                    <ul class="list-inline">
                                        <li class="list-inline-item">
                                            <a href="{{ route("users.edit", $user->id) }}">
                                                <i class="fa fa-pencil"></i>
                                            </a>
                                        </li>
                                        <li class="list-inline-item">
                                            <a href="{{ route("users.assign-orgs", $user->id) }}">
                                                <i class="fa fa-hospital-o"></i>
                                            </a>
                                        </li>
                                        <li class="list-inline-item">
                                            {{ Form::model($user, ['route' => ['users.destroy', $user->id], "onClick" => "javascript:return window.confirm('Are you sure?')"]) }}
                                                {{ Form::hidden("_method", "DELETE") }}
                                                <button class="btn btn-link text-default">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            {{ Form::close() }}
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    @else

        <div class="text-center">
            <p class="lead">There are no users</p>
            <a href="{{ route('users.create') }}" class="btn btn-primary">Create first user</a>
        </div>

    @endif
@stop