@extends('layouts.app')

@section("page_title", "Grant Organization Access to {$user->name}")

@section('breadcrumbs')
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">Home</a></li>
    <li class="breadcrumb-item"><a href="{{ route("users.index") }}">Users</a></li>
    <li class="breadcrumb-item active">Grant Organization Access to {{ $user->name }}</li>
</ol>
@stop

@section("content")
<div class="card">
    <div class="card-body">
        {{ Form::model($user, ['route' => ['users.assign-orgs', $user]], ["class" => "form"]) }}

            @foreach($orgs as $org)
            <div class="form-group" style="margin-bottom: 0;">
                <label class="checkbox">
                    {{ Form::checkbox('organizations[]', $org->id, in_array($org->id, $user_orgs), ['class' => 'checkbox']) }}
                    {{ $org->name }}
                </label>
            </div>
            @endforeach

            <button class="btn btn-primary">Save changes</button>
            <a href="{{ route("users.index") }}" class="btn btn-link">Cancel</a>

        {{ Form::close() }}
    </div>
</div>
@stop