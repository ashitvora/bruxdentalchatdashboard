<?php

Auth::routes();

Route::get('/', 'HomeController@index')->name('home')->middleware("auth:web");

Route::group([
	"middleware" => "auth:web",
], function () {
	Route::resource("organizations", "OrganizationsController");
	Route::get("organizations/{organization}/users", "OrganizationsController@assignUsers")->name("organizations.assign-users");
	Route::post("organizations/{organization}/users", "OrganizationsController@updateUsers")->name("organizations.assign-users");

	Route::resource("organizations.chats", "ChatController");

	Route::resource("users", "UsersController");
	Route::get("users/{user}/organizations", "UsersController@assignOrganizations")->name("users.assign-orgs");
	Route::post("users/{user}/organizations", "UsersController@updateOrganizations")->name("users.assign-orgs");

	Route::get("chats/all", "ChatController@all")->name("chats.all");
});